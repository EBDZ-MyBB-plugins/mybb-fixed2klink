<?php

$plugins->add_hook('parse_message_start', 'fixed2klink_parse_message_start',1);
$plugins->add_hook('parse_message_end', 'fixed2klink_parse_message_end',1);

function fixed2klink_info()
{
	return array(
		"name"				=> "Fix ed2k link",
		"description"		=> "Allow to put [ and ] in ed2k link and remove the blank target",
		"website"			=> "",
		"author"			=> "Betty",
		"authorsite"		=> "",
		"version"			=> "0.1.0",
		"codename"			=> "fixed2klink",
		"compatibility"		=> "*",
	);
}
function fixed2klink_parse_message_start($message) {
	/* fix [] for ed2k filename */
	$message = preg_replace_callback('#(\[url=ed2k\:\/\/\|file\|)(.*?)(\|[0-9]*)(.*?)(\])(.*?)(\[/url\])#is', function($matches) {
		return $matches[1] . str_replace(['[',']'], ['%5B','%5D'],$matches[2]) . $matches[3] . $matches[4] . $matches[5] . $matches[6] . $matches[7];
	}, $message);
	/* fix [] for ed2k filename in line*/
	$message = preg_replace_callback('#(\[lien=ed2k\:\/\/\|file\|)(.*?)(\|[0-9]*)(.*?)(\])(.*?)(\[/lien\])#is', function($matches) {
		return "[ed2k=ed2k://|file|" . str_replace(['[',']'], ['%5B','%5D'],$matches[2]) . $matches[3] . $matches[4] . $matches[5] . $matches[6] . "[/ed2k]";
	}, $message);
	/* fix [] for ed2k filename in line*/
	$message = preg_replace_callback('#(\[ed2k=ed2k\:\/\/\|file\|)(.*?)(\|[0-9]*)(.*?)(\])(.*?)(\[/ed2k\])#is', function($matches) {
		return $matches[1] . str_replace(['[',']'], ['%5B','%5D'],$matches[2]) . $matches[3] . $matches[4] . $matches[5] . $matches[6] . $matches[7];
	}, $message);
	/* Need create myblockurl tag manually */
	/* remove [url]…[/url] */
	$message = preg_replace_callback('#\[url\](.*?)\[/url\]#is', function($matches) {
		return "[myblockurl]" . $matches[1] . "[/myblockurl]";
	}, $message);
	/* remove [url=#]…[/url] */
	$message = preg_replace_callback('#\[url=\#\](.*?)\[/url\]#is', function($matches) {
		return "[myblockurl]" . $matches[1] . "[/myblockurl]";
	}, $message);
	return $message;
}
function fixed2klink_parse_message_end($message) {
	/* fix ed2k link with _blank */
	$message = preg_replace('#<a(.*?)href="ed2k://(.*?)"(.*?)target="_blank"(.*?)>#is', '<a$1href="ed2k://$2"$3$4>', $message);
	//$message = preg_replace('#\[myblockurl\](.*?)[/myblockurl\]#is', '<div class="mycode_url mycode_false_url">$1</div>', $message);
	return $message;
}
